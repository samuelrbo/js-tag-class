/**
 * Tag Class
 * 
 * Ajuste no css para o container nao ficar vuando (retirado float:left, remover do arquivo .css)
 * adição do input com os valores das tags para o comit do form
 *
 * @since 2014-05-08 13:34
 * @version 1.0
 * @author Samuel Ramon <samuelrbo@gmail.com>
 * @site https://bitbucket.org/samuelrbo/js-tag-class
 * @site https://github.com/samuelrbo/js-tag-class
 *
 * @param params {id,arrTags,css,placeholder,normalize}
 * @returns {Tag}
 */
function Tag(params) {
	// Class attributes
	var id = params.id; // container ID
	var arrTags = params.arrTags; // initial tags
	var css = params.css; // style sheet for tags
	var placeholder = (params.placeholder) ? params.placeholder : 'New tag'; // place holder for add new tag
	var normalize = (params.normalize) ? params.normalize : false; // normalize tag text, remove spaces, special chars and accents

	// Simple object for DOM manipulation
	var el = {
		byId: function(id) {
			return document.getElementById(id);
		},
		byTag: function(tagName) {
			return document.getElementsByTagName(tagName);
		},
		// Fast way to create element with attributes
		n: function(element, attributes) {
			var elem = document.createElement(element);
			for(var attr in attributes) {
				elem[attr] = attributes[attr];
			}
			return elem;
		}
	};

	// Getting container DOM Element
	var container = el.byId(id);
	// trating firefox innerHtml and innerText Problem
	var textAttr = (navigator.userAgent.indexOf("Firefox") != -1) ? 'textContent' : 'innerText'

	// Check if style sheet was configured, in case of false it will use the default style
	if (css != null && css != 'undefied') {
		el.byTag('head')[0].appendChild(el.n('link',{
			'rel':'stylesheet',
    		'href':css
		}));
	} else {
		// Check if CSS already loaded
		if(!el.byId('tag-css')) {
			// Create the style head container
			el.byTag('head')[0].appendChild(el.n('style',{
				'type':'text/css',
				'id':'tag-css'
			}));
		}

		// Default style for tags, use CSS3 to create the X button
		el.byId('tag-css')[textAttr] = el.byId('tag-css')[textAttr] + 
		'#'+id+'{ border:1px solid #ccc; padding:5px; font-family:Arial; }'+ 
		'#'+id+' span.elTag{ cursor:pointer; display:block; float:left; color:#fff;'+
			'background:#689; padding:5px; padding-right:25px; margin:4px; border-radius:5px; }'+
		'#'+id+' span.elTag:hover{ opacity:0.7; }'+
		'#'+id+' span.elTag:after{ position:absolute; content:"x"; border:1px solid;'+ 
			'padding:0 4px 2px; margin:1px 0 10px 5px; font-size:10px; }'+
		'#'+id+' input{ background:#eee; border:0; margin:4px; padding:7px; width:auto; }'
	}

	var hiddenInput = el.n('input',{
		'value':'',
		'type':'hidden',
		'name':'input_tag',
	});
	container.appendChild(hiddenInput);
	
	// Check if was informed the initial tags to begging in the container
	if (arrTags instanceof Array) {
		// Reversing the array to be inserted in the container in the same order of the array
		arrTags.reverse().forEach(function(tag,idx){
			hiddenInput.value += tag+",";
			var span = el.n('span',{
				'className':'elTag',
				'innerText':tag,
				'onclick':function(){
					var that = this;
					var newVal = "";
					hiddenInput.value.split(",").forEach(function(v,i){
						if (v.trim() !== "" && v != that[textAttr].trim()) {
							newVal += v+",";
						}
					});
					hiddenInput.value = newVal;
					this.remove();
				}
			});
			span[textAttr] = tag;

			// Creating the span element for tags
			container.appendChild(span);
		});
	}

	// Creating and insert the input to add tags in the container
	container.appendChild(el.n('input',{
		'type':'text',
		'value':'',
		'id':'input_tag',
		'placeholder':placeholder,
		'onkeyup': function(e) {
			// key event for insert the word in the input to be a tag
			if (e.keyCode === 188 || e.keyCode === 13) { // comma or enter action
				var txt = this.value.replace(',','').trim(); // removing the comma if exists and trim spaces
				if (txt != null && txt != '') {
					if (normalize) { // if normalized is setted
						// replacing special chars, spaces and accents
						txt = txt.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-').replace(/^(-)+|(-)+$/g,'')
								 .replace(new RegExp('[ÁÀÂÃ]','gi'),'a').replace(new RegExp('[ÉÈÊ]','gi'),'e')
								 .replace(new RegExp('[ÍÌÎ]','gi'),'i').replace(new RegExp('[ÓÒÔÕ]','gi'),'o')
								 .replace(new RegExp('[ÚÙÛ]','gi'),'u').replace(new RegExp('[Ç]','gi'),'c').toLowerCase();
					}
					
					hiddenInput.value += txt+",";
				
					var span = el.n('span', {
						'className':'elTag', 
						'onclick': function(){
							var that = this;
							var newVal = "";
							hiddenInput.value.split(",").forEach(function(v,i){

								if (v.trim() !== "" && v != that[textAttr].trim()) {
									newVal += v+",";
								}
							});
							hiddenInput.value = newVal;
							this.remove();
						}
					});
					span[textAttr] = txt;

					// creating the span element with the input word and insert in the container tags
					container.insertBefore(span,this);
				}
				// reset the input value
				this.value = '';
			}
		},
		'onkeypress': function(e) {
			if (e.keyCode === 188 || e.keyCode === 13) {
				e.preventDefault();
			}
		}
	}));
};