# tag.class.js

This class use pure Javascript to create a tag field container.

##DONE:
[2014-02-24]

  * Fixed Firefox innerHTML problem;
  * Fixed form submission when pressed enter on input tag;
  * Change the auxiliar class;
  * Fixed the creation of a style tag in HTML head for each tag input;
  * Adjusted the example in index.html;

# Usage

Call the class in your projects <head>

```html
<script src="tag.class.js"></script>
<!-- OR -->
<script src="tag.class.min.js"></script>
```

## Create

Using the simple way, only set the container id

```html
<!-- The tag container: Example 01 -->
<div id="tags"></div>
```

```js
new Tag({'id':'tags'});
```

OR setting up the other parameters

```html
<!-- The tag container: Example 02 -->
<div id="container_test"></div>
```

```js
new Tag({
	'id':'container_test',
	'arrTags':['t1','t2','t3'],
	'placeholder':'Insert Tag',
	'css':'style.css',
	'normalize':true
});
```

# License

MIT-License (see LICENSE file).


# Contributing

Your contribution is welcome.


# TODO

  * Provide a live example
  * Way to use HTML for create the remove button, not only CSS3
  * Validate in other browsers, only tested in CHROME and FIREFOX
  * Include Unit Tests
